# Overview

Every Chef Infra installation needs a Chef Repository. This is the place where cookbooks, policyfiles, config files and other artifacts for managing systems with Chef Infra will live. We strongly recommend storing this repository in a version control system such as Git and treating it like source code.

## How this repository was started

```bash
chef generate repo learnchef-repo
chef generate cookbook cookbooks/learnchef
```

## Repository Directories

This repository contains several directories, and each directory contains a README file that describes what it is for in greater detail, and how to use it for managing your systems with Chef.

- `cookbooks/` - Cookbooks you download or create.
- `data_bags/` - Store data bags and items in .json in the repository.
- `roles/` - Store roles in .rb or .json in the repository.
- `environments/` - Store environments in .rb or .json in the repository.

## Configuration

The config file, `.chef/config.rb` is a repository-specific configuration file for the knife command line tool. If you're using the Hosted Chef platform, you can download one for your organization from the management console. You can also generate a new config.rb by running `knife configure`. For more information about configuring Knife, see the Knife documentation at https://docs.chef.io/workstation/knife/

## knife commands

knife is the CLI that can be used to deal with target systems and chef Infra Server. Some commands:

* knife bootstrap - Initialize target systems, installing chef-client, register the servers on Chef Infra Server, configuring some policies runlits, secret keys, etc...
* knife node - Allows to execute runlsit remotelly, update the Policies and view informations about the nodes.
* knife ssh - Access the nodes trough ssh

## Next Steps

Read the README file in each of the subdirectories for more information about what goes in those directories.
